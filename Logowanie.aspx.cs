﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logowanie : System.Web.UI.Page
{
    //dzięki temu connectionStringa nie trzeba zmieniać
    private static string path = HttpContext.Current.Server.MapPath("~/App_Data/DziekanatDB.mdf");
    protected void Page_Load(object sender, EventArgs e)
    {
        Logiczna.Administrator = false;
        Logiczna.Student = false;
        Logiczna.Wykladowca = false;
        Label3.Visible = false;
        haslo.TextMode = TextBoxMode.Password;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Sess
        string login = TextBox1.Text;
        string haslo = this.haslo.Text;
        if (login != "" && haslo != "")
        {
            if (SprawdzAdministratora(login, haslo))
            {
                Session["administratorLogin"] = login;
                Session["administratorHaslo"] = haslo;
                Logiczna.Administrator = true;
                Response.Redirect("PROTECTED/AdminLayer/StronaAdmin.aspx");
            }
            else if (SprawdzStudenta(login, haslo))
            {
                Session["studentLogin"] = login;
                Session["studentHaslo"] = haslo;
                Logiczna.Student = true;
                Response.Redirect("PROTECTED/StudentLayer/StronaStudent.aspx");

            }
            else if (SprawdzWykladowce(login, haslo))
            {
                Session["wykladowcaLogin"] = login;
                Session["wykladowcaHaslo"] = haslo;
                Logiczna.Wykladowca = true;
                Response.Redirect("PROTECTED/WykladowcaLayer/StronaWykladowca.aspx");
            }
            else
            {
                Logiczna.Administrator = false;
                Logiczna.Student = false;
                Logiczna.Wykladowca = false;
                Label3.Visible = true;
            }

        }
    }
    private bool SprawdzAdministratora(string login, string haslo)
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select imie from Administratorzy where Administratorzy.login = '{0}' and Administratorzy.haslo = '{1}'", TextBox1.Text, this.haslo.Text);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        connection.Close();
        if (dt.Rows.Count != 0)
        {
            return true;
        }
        return false;
    }
    private bool SprawdzStudenta(string login, string haslo)
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select imie from Studenci where Studenci.nr_indeksu = '{0}' and Studenci.haslo = '{1}'", TextBox1.Text, this.haslo.Text);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        connection.Close();
        if (dt.Rows.Count != 0)
        {
            return true;
        }
        return false;
    }
    private bool SprawdzWykladowce(string login, string haslo)
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select imie from Wykladowcy where Wykladowcy.login = '{0}' and Wykladowcy.haslo = '{1}'", TextBox1.Text, this.haslo.Text);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        connection.Close();
        if (dt.Rows.Count != 0)
        {
            return true;
        }
        return false;
    }
}